# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=freetds
pkgver=1.4.16
pkgrel=0
pkgdesc="Tabular Datastream Library"
url="https://www.freetds.org/"
arch="all"
license="GPL-2.0-or-later OR LGPL-2.0-or-later"
makedepends="openssl-dev>3 linux-headers readline-dev unixodbc-dev"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"
source="https://www.freetds.org/files/stable/freetds-$pkgver.tar.bz2"
options="!check"  # tests require running SQL server http://www.freetds.org/userguide/confirminstall.htm#TESTS

prepare() {
 	default_prepare
	update_config_sub
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--enable-msdblib \
		--with-openssl=/usr \
		--enable-odbc \
		--with-unixodbc=/usr
	make
}

check() {
	cd "$builddir"/src/replacements
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

sha512sums="
7c6ce7bee96f639bb06164c1ffe2411cf38eb146086dc9383be92edb96ff3e300bae8ead8d4f5aefcb5e5baeaf0306cd8e9f2f6d154a47c2116e939810b2f856  freetds-1.4.16.tar.bz2
"
