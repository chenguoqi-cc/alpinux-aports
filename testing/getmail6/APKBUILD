# Contributor: Henrik Riomar <henrik.riomar@gmail.com>
# Maintainer: Henrik Riomar <henrik.riomar@gmail.com>
pkgname=getmail6
pkgver=6.19.00
pkgrel=0
pkgdesc="mail retriever with support for POP3, IMAP4 and SDPS"
url="https://getmail6.org/"
arch="noarch"
license="GPL-2.0-only"
checkdepends="py3-pytest"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
depends="python3"
subpackages="$pkgname-doc $pkgname-pyc"
source="https://github.com/getmail6/getmail6/archive/v$pkgver/getmail-$pkgver.tar.gz"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -v test/test.py
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
	mv "$pkgdir/usr/share/doc/getmail-$pkgver" "$pkgdir/usr/share/doc/$pkgname"
}

sha512sums="
ce19d2595d99dd508966f30000b47985eee1636d0d4e4f4431bf8fde3b323c097a9a052512a65536cc7a34b2bc2ab528def1a9b3a616666ad810765692145546  getmail-6.19.00.tar.gz
"
